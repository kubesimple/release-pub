.PHONY: fmt build protobuf

default: build

fmt:
	./build-tools/scripts/check --fmt

gencerts:
	./build-tools/scripts/gencerts

build:
	./build-tools/scripts/build -p 3

run.server:
	./bin/release-pub_linux_amd64 server \
		--log-level debug \
		--cert-file bin/tls/cert.pem \
		--key-file bin/tls/key.pem

protobuf:
	protoc -I/usr/local/include \
		-I$$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--proto_path=pkg/proto \
		--go_out=plugins=grpc:pkg/proto \
		publisher.proto
	protoc -I/usr/local/include \
		-I$$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--proto_path=pkg/proto \
		--grpc-gateway_out=logtostderr=true:pkg/proto \
		publisher.proto
