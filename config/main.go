package config

import (
	log "github.com/sirupsen/logrus"
)

type Server struct {
	Port      string `mapstructure:"port"`
	LogLvl    string `mapstructure:"log-level"`
	CertFile  string `mapstructure:"cert-file"`
	KeyFile   string `mapstructure:"key-file"`
	QueueAddr string `mapstructure:"queue-addr"`
}

// LogLevel returns the log.Level type.
func (s *Server) LogLevel() log.Level {
	var l log.Level
	switch s.LogLvl {
	case "info":
		l = log.InfoLevel
	case "warn":
		l = log.WarnLevel
	case "error":
		l = log.ErrorLevel
	case "debug":
		l = log.DebugLevel
	default:
		l = log.InfoLevel
	}
	return l
}
