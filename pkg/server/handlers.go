package server

import (
  "context"
  "github.com/streadway/amqp"
)

type handler struct{}

func (h *handler)Publish(ctx context.Context, rb *bb.ReleaseBundle) error {
  conn, err := amqp.Dial("amqp://guest:guest@localhost:5672")
  if err != nil {
    return err
  }
  defer conn.Close()

  ch, err := conn.Channel()
  if err != nil {
    return err
  }

  // todo: this should probably be done at startup, not in the handler
  q, err := ch.QueueDeclare()
  return ch.Publish()
}


