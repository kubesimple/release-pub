package server

import (
	"bitbucket.org/kubesimple/release-pub/config"
	pb "bitbucket.org/kubesimple/release-pub/pkg/proto"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"net"
	"os"
	"os/signal"
	"syscall"
)

func Start(cfg *config.Server) error {
	initLogger(cfg.LogLevel())

	log.Debugf("log level: %s\n", cfg.LogLvl)
	log.Debugf("gRPC port: %s\n", cfg.Port)
	log.Debugf("server certificate: %s\n", cfg.CertFile)
	log.Debugf("server key: %s\n", cfg.KeyFile)
	log.Debugf("queue address: %s\n", cfg.QueueAddr)

	lis, err := net.Listen("tcp", net.JoinHostPort("", cfg.Port))
	if err != nil {
		return err
	}
	defer lis.Close()

	tls, _ := credentials.NewServerTLSFromFile(cfg.CertFile, cfg.KeyFile)
	opts := []grpc.ServerOption{
		grpc.Creds(tls),
	}
	grpcServer := grpc.NewServer(opts...)

	srv := &handler{}
	pb.RegisterGatewayServer(grpcServer, srv)

	done := make(chan os.Signal, 1)
	signal.Notify(done, syscall.SIGINT, syscall.SIGTERM)

	log.Infof("starting gRPC server\n")
	go grpcServer.Serve(lis)

	<-done
	log.Infof("stopping gRPC server\n")
	grpcServer.Stop()
	return nil
}

func initLogger(level log.Level) {
	log.SetLevel(level)
}
